import itertools

# counter func 
# counter = itertools.count(start=5, step=-2.5)

# print(next(counter))
# print(next(counter))
# print(next(counter))
# print(next(counter))
# print(next(counter))
# print(next(counter))

# data = [100, 200, 300, 400]
# daily_data = list(zip(itertools.count(), data))
# print(daily_data)

# zip longest
data = [100, 200, 300, 400]
daily_data = list(itertools.zip_longest(range(10), data))
print(daily_data)

