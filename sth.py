from screeninfo import get_monitors

for m in get_monitors():
    if m.is_primary:
        print(m.width)
        print(m.height)