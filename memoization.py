import time

ef_cache = {}


def expensive_func(num):
    if num in ef_cache:
        return ef_cache[num]
    print(f"Computing {num}...")
    time.sleep(1)
    result = num * num
    ef_cache[num] = result
    return result


start_time = time.time()

result = expensive_func(4)
print(result)

result = expensive_func(10)
print(result)

result = expensive_func(4)
print(result)

result = expensive_func(10)
print(result)

print(f"Executing time: {time.time() - start_time}")