import datetime

current_weight = 65
goal_weight = 75
average_kg_per_week = 0.5

start_date = datetime.datetime.today()
end_date = start_date

while current_weight < goal_weight:
    end_date += datetime.timedelta(days=7)
    current_weight += average_kg_per_week

print(f'Reached goal in {(end_date - start_date).days // 7} weeks.')
