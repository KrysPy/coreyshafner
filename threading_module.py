import concurrent.futures
import threading
import time 


start = time.perf_counter()

def do_something(sleeping_time):
    print(f"Sleeping {sleeping_time} second...")
    time.sleep(sleeping_time)
    return "Done sleeping..."
    
with concurrent.futures.ThreadPoolExecutor() as executor:
    secs = [5, 4, 3, 2, 1]

    results = executor.map(do_something, secs)
    
    for result in results :
        print(result )

# threads = []

# for _ in range(1):
#     th = threading.Thread(target=do_something, args=[3])
#     th.start()
#     threads.append(th)
    
# for thread in threads:
#     thread.join()

finish = time.perf_counter()

print(f"Finished in {round(finish-start, 2)} second(s)")
