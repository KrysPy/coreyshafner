_list = [1, 2, 3, 4]

for i in _list:
    print(i)
    if i == 3:
        break
else:
    print("Hit the for/else statement")

print()
for i in _list:
    print(i)
    if i == 6:
        break
else:
    print("Hit the for/else statement")

print("Practical example!!!")


def find_index(to_search, target):
    for i, value in enumerate(to_search):
        if value == target:
            break
    else:
        return -1
    return i


my_list = ['Corey', 'Rick', 'John']
index_location = find_index(my_list, 'Rick')

print(f"Location of target is index: {index_location}")