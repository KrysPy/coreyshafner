import csv

html_output = ''
names = []

with open('patrons.csv', 'r') as csv_file:
    csv_read = csv.DictReader(csv_file)

    # we use next because we don't want to first line of data
    next(csv_read)

    for line in csv_read:
        # after no reward we don't want to display
        if line['FirstName'] == "No Reward":
            break
        full_name = '{} {}'.format(line['FirstName'], line['LastName'])
        names.append(full_name)

for num, name in enumerate(names):
    print(f"{num + 1}. {name}")

print()
html_output += f"<p>There is {len(names)} in list.</p>"

html_output += '\n<ul>'

for name in names:
    html_output += f"\n\t<li>{name}</li>"

html_output += '\n</ul>'

print(html_output)