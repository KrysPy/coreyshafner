import builtins

# global and local scope

# # it is a global scope because it is in main body of our file
# x = 'global x'
#
#
# def test():
#     # it is local variable for test function
#     # when we want to overwrite global variable in function we first have to:
#     global x
#     x = 'local x'
#     print(x)
#
#
# test()
# print(x)


# _________________________________________________________________________________________________________
# local variable as argument in function
# def test(z):
#     # it is local variable for test function
#     # when we want to overwrite global variable in function we first have to:
#     x = 'local x'
#     print(z)
#
#
# test('local z')

# _________________________________________________________________________________________________________
# built-in function

# m = min([5, 1, 4, 2, 3])
# print(m)
# print(dir(builtins))

# _________________________________________________________________________________________________________
# enclosing

def outer():
    # this is local variable for out outer function
    x = 'outer x'

    def inner():
        # this is local for out inner function
        # with nonlocal statement we now affecting to x in outer function - it is
        nonlocal x
        # x = 'inner x'
        print(x)

    inner()
    print(x)


outer()