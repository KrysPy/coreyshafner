import csv

# with open('names.csv', 'r') as csv_file:
#     csv_reader = csv.reader(csv_file)
#
#     # to loop over first LINE!!!!!
#     next(csv_reader)
#
#     with open('new_names.csv', 'w') as new_file:
#         csv_writer = csv.writer(new_file, delimiter='\t')
#
#         for line in csv_reader:
#             csv_writer.writerow(line)

# dictionary reader and writer
with open('names.csv', 'r') as csv_file:
    csv_reader = csv.DictReader(csv_file)

    with open('new_names.csv', 'w') as new_file:
        field_names = ['first_name', 'last_name', 'email']

        csv_writer = csv.DictWriter(new_file, fieldnames=field_names, delimiter='\t')
        csv_writer.writeheader()
        for line in csv_reader:
            del line['email']       # in this way we can remove some field from csv file
            csv_writer.writerow(line)