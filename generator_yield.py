# Generator Expressions

# +++++++++ PART I +++++++++
# I want to yield 'n*n' for each 'n' in nums
nums = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]


# def gen_func(nums):
#     for n in nums:
#         yield n * n
#
#
# my_gen = gen_func(nums)

my_gen = (n * n for n in nums)

for i in my_gen:
    print(i)

# +++++++++ PART II +++++++++


def square_numbers(nums):
    result = []
    for i in nums:
      yield i * i


my_nums = square_numbers([1, 2, 3, 4, 5])

print(next(my_nums))
print(list(my_nums))