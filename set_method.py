# to create empty set we use set() because {} will create a function

s1 = {1, 1, 1, 2, 3, 4, 5}
print(s1)
print()
s1.add(6)
print(s1)
print()
s1.update([6, 4, 6, 7])
print(s1)
print()
