# DRAW CHALLENGE AND RUN WEBSITE
from bs4 import BeautifulSoup
import requests


with open('simple.html') as html_file:
    soup = BeautifulSoup(html_file, 'lxml')

# print(soup.prettify())
match = soup.title.text
print(match, '\n')
match = soup.div
print(match, '\n')
# we pass argument class_=, because we can match specify text
# we use underscore because class is keyword on Python
# article = soup.find('div', class_='article')
for article in soup.find_all('div', class_='article'):
    headline = match.h2.a.text
    print(headline)
    summary = match.p.text
    print(summary, '\n')
