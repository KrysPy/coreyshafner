import datetime

class Employee:
    
    num_of_emps = 0 
    raise_amount = 1.04
    
    def __init__(self, first_name: str, last_name:str, pay) -> None:
        self.first_name = first_name
        self.last_name = last_name
        self.pay = pay
        self.email = first_name + "." + last_name + "@domain.com"
        Employee.num_of_emps += 1
        
    def display_fullname(self): 
        return f"{self.first_name} {self.last_name}"
    
    def apply_raise(self):
        self.pay = int(self.pay * self.raise_amount)
    
    @classmethod
    def set_raise_amt(cls, amount):
        cls.raise_amount = amount

    @classmethod
    def from_string(cls, emp_str):
        first, last, pay = emp_str.split('-')
        return cls(first, last, pay)
    
    @staticmethod
    def is_workday(day: datetime):
        if day.weekday() == 5 or day.weekday() == 6:
            return False
        return True
    

if __name__ == "__main__":
    emp_1 = Employee("Krystian", "Banach", 10000)
    emp_2 = Employee("Test", "User", 60000)

    emp_str_1 = "John-Doe-70000"
    emp_str_2 = "Steve-Smith-30000"
    emp_str_3 = "Jane-Doe-90000"

    new_emp_1 = Employee.from_string(emp_str_1)

    emp_1.set_raise_amt(1.05)
    
    print(Employee.raise_amount)
    print(emp_1.raise_amount)
    print(emp_2.raise_amount)
    
    my_date = datetime.date(2016, 7, 11)
    print(Employee.is_workday(my_date))
