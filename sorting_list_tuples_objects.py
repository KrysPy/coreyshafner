from operator import attrgetter

lst = [4, 2, 5, 7, 8, 9, 1]
lst.sort()      # when we don't want to create new variable we use method .sort() (JUST FOR LISTS)
print(lst)
lst = [4, 2, 5, 7, 8, 9, 1]
new = sorted(lst, reverse=True)     # we can add reverse parameter also in sort method
print(new)
# we can use sorted() function to sort all data types (tuples, dict etc.)


class Employy():
    def __init__(self, name, age, pay):
        self.name = name
        self.age = age
        self.pay = pay

    def __repr__(self):
        return "{}, {} $ {}".format(self.name, self.age, self.pay)


e1 = Employy("John", 45, 35000)
e2 = Employy("Ben", 34, 35000)
e3 = Employy("Ron", 32, 35000)

# we can use operator library to sort objects in simple way,
# if we didn't have that lib we would have to write new function (for e.g. using lambda)
lst_eml = [e1, e2, e3]
new = sorted(lst_eml, key=attrgetter('name'))
print(new)


