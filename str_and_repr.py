import datetime
import pytz

# difference between str and repr
# The goal of __repr__ is to be unambiguous

# The goal of __str__ is to be readable

# when we use str we get sth readable but just on "surface", we don't know what kind of object it is
# when use repr we get unambiguous form with kind of object on the beginning

a = datetime.datetime.utcnow().replace(tzinfo=pytz.UTC)

b = str(a)

print(f'str(a): {str(a)}')
print(f'str(b): {str(b)}')

print()

print(f'repr(a): {repr(a)}')
print(f'repr(b): {repr(b)}')

"""
    If debugging sth and that not work as should so we can use repr
    When sth has to be non-programmer friendly we should use str cause it's readable
"""