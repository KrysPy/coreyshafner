"""
    Whene we want to make dunder methods ass add,len, str etc. on our class object we have to
    create them in our class as a method

"""

from this import d


class Employee: 
    
    raise_amt = 1.04
    
    def __init__(self, first, last, pay) -> None:
        self.first = first
        self.last = last
        self.pay = pay
        self.email = first + "." + last + "@gmail.com"
    
    def fullname(self):
        return f"{self.first} {self.last}"
    
    def apply_raise(self):
        self.pay = int(self.pay * self.raise_amt)
        
    def __repr__(self) -> str:
        return f"Employee('{self.first}', '{self.last}', '{self.pay}')"    
    
    def __str__(self) -> str:
        return f"{self.fullname} - {self.email}"
    
    def __add__(self, other):
        return self.pay + other.pay
    
    def __len__(self):
        return len(self.fullname())
    
    
emp_1 = Employee("Corey", "Schafer", 50000)
emp_2 = Employee("Test", "Empl", 60000)


emp1 = Employee("Corey", "Shafer", 60000)
emp2 = Employee("Kim", "Shafer", 50000)

print(emp1.__repr__())
print(emp1.__str__())
print()
print(str(emp1))
print(repr(emp1))
print()
print(1 + 2)
print(int.__add__(1, 2))        # we can do the same with strings
print()
print(emp1 + emp2)      # we can do that thanks to created method __add__
print()
print(len("test"))
print('test'.__len__())
print()
print(emp1.__len__())
print()

print(emp_1 + emp_2)
print(emp_1.__repr__())
print(emp_1.__str__())
print(len(emp_2))
