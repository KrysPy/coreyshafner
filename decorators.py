"""
Decorator - is a function that takes another function as an argument and then returns another function
Decorate our function allow us to easily add functionality to our existing function by adding that functionality
inside of out wrapper

"""


def decorator_function(original_function):
    def wrapper_function(*args, **kwargs):
        print("Wrapper executed this before {}".format(original_function.__name__))
        return original_function(*args, **kwargs)
    return wrapper_function


# We can also use classes as decorators
# class DecoratorClass(object):
#
#     def __init__(self, original_function):
#         self.original_function = original_function
#
#     def __call__(self, *args, **kwargs):
#         print("call method executed this before {}".format(self.original_function.__name__))
#         return self.original_function(*args, **kwargs)


@decorator_function
def display():
    print("Display function")


@decorator_function
def display_info(name, age):
    print("display_info ran with arguments ({}, {})".format(name, age))


display_info("John",  24)
display()

"""
@decorator_function
def display():
    print("Display function")

IS THE SAME AS:

display = decorator_function(display)

"""

# ____________________________________________________________




