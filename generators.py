
# creatig and working with list
# def square_numbers_list(nums):
#     result = []
#     for i in nums:
#         result.append(i * i)
#     return result 

# my_nums_list = square_numbers_list([1, 2, 3, 4, 5])
# we can replace that function with list comprehension

my_nums_list = [i * i for i in [1, 2, 3, 4, 5]]

print(my_nums_list)


# -----------------------------------------------------------------------------------------
# now making a generator of it
# def square_numbers_generator(nums):
#     for i in nums:
#         yield (i * i)
# my_nums_generator = square_numbers_generator([1, 2, 3, 4, 5])

# we can replace that function with generator one liner

my_nums_generator = (i * i for i in [1, 2, 3, 4, 5])


print(my_nums_generator) 

# to print all values from generator we can convert generator to a list
generator_to_list = list(my_nums_generator)

print(generator_to_list)