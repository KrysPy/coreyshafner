from getpass import getpass

# one liner
condition = True

x = 1 if condition else 0
print(x)

# -------------------------------------------------------------------------------------
# big numbers - adding underscores which not affect to program
num1 = 1_000_000_000
num2 = 1_000_000
total = num1 + num2

print(f'{total: ,}')        # formatting long numbers!!!

# -------------------------------------------------------------------------------------
# opening and closing files - is better to use context manager (for manage resources)
# context managers also good to use with threads and databases
f = open('Test.txt', 'r')
file_content = f.read()
f.close()
words = file_content.split(' ')
word_count = len(words)
print(word_count)

# -------------------------------------------------------------------------------------
# automatic numerating elements of list
names = ['Corey', 'Chris', 'Dave', 'Travis']
for index, name in enumerate(names):
    print(f'Name: {name} has index: {index}')

# -------------------------------------------------------------------------------------
# access to two lists at the same loop with zip function
# we can use zip function for more than 2 lists
names2 = ['Peter', 'Clark', 'Wade', 'Bruce']
heroes = ['S', 'Spr', 'D', 'B']

for name, hero in zip(names, heroes):
    print(f'{name} is actually {hero}')

# -------------------------------------------------------------------------------------
# unpacking
# when we put * before last element all of the unpacked values will be at this variable with *
# when we put _* we ignore rest of unpacked values, first values will be assigned
# we can put this *x or *_ at the middle list and there will be not assigned values
# we can use it in multiple places
items = (1, 2)
items2 = (1, 2, 3, 5, 6, 7, 8)
a, b = items
c, d, *e = items2
print(a)
print(b)
print(c)
print(d)
print(e)


# -------------------------------------------------------------------------------------
# getting and setting attribute of certain object
# SETATTR and GETATTR
class Person:
    pass


person = Person()
# first way to do this
# person.first = 'Corey'
# person.last = 'Schafer'

# second way to do this
person_info = {'first': 'Corey', 'last': 'Schafer'}
for key, value in person_info.items():
    setattr(person, key, value)

for key in person_info.keys():
    print(getattr(person, key))

# -------------------------------------------------------------------------------------
# input secret information
username = input('Username:')
password = getpass('Password:')

print('Logging in... ')

# -------------------------------------------------------------------------------------
# -m running specify module /
