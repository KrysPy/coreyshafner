first_name = "Krys"
second_name = "Py"
person = {'Name': 'Krys', 'age': 23}

# sentence = 'My name is {} {}'.format(first_name, second_name)
# print(sentence)
#
# sentence = f'My name is {first_name.upper()} {second_name}'     # more elegant and intuitive than this above
# print(sentence)

# work with DICT
sentence = f"My name is {person['Name']} {person['age']}"
print(sentence)

# we can do calculations
calculation = f'4 times 11 is equal to {4 * 11}'
print(calculation)

# formatting numbers
for n in range(1, 11):
    sentence = f'The value is {n:02}'       # we put 02
    print(sentence)

# floating point value
pi = 3.14244556
sentence = f'Pi is equal to {pi:.3f}'   # . than precision than floating
print(sentence)

# FORMATTING DATA!!!!
from datetime import datetime
birthday = datetime(1990, 1, 1)
sentence = f'Jenn has bithday on {birthday:%B %d, %Y}'
print(sentence)
