# Difference between '==' and 'is'
# '==' checks for equality, checks if values are equal
# 'is' checks for identity, checks if the values are identical in terms of being the same object in memory
#           That checks if objects are specify the same objects

# So 'is' is for exactly the same objects (in memory), e.g. when we change element in one element the same element will
#       change in another object

l1 = [1, 2, 3, 4, 5]
l2 = l1

print(id(l1))       # thanks to 'id' we get memory location
print(id(l2))

if l1 is l2:
    print(True)
