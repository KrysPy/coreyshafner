import memory_profiler
import random
import time

names = ['John', 'Corey', 'Adam', 'Steve', 'Rick', 'Thomas']
majors = ['Math', 'Engineering', 'CompSci', 'Arts', 'Business']

print(f"Memory (Before): {memory_profiler.memory_usage()} Mb")

def people_list(num_people):
    result = []
    for i in range(num_people):
        person = {
            'id': i,
            'name': random.choice(names),
            'major': random.choice(majors)
        }
        result.append(person)
    return result

def people_generator(num_people):
    for i in range(num_people):
        person = {
            'id': i,
            'name': random.choice(names),
            'major': random.choice(majors)
        }
        yield person
        
# t1 = time.time()
# people = people_list(1_000_000)
# t2 = time.time()

t1 = time.time()
people = people_generator(1000000)
t2 = time.time()

print(f"Memory (After): {memory_profiler.memory_usage()} Mb")
print(f"Took {t2 - t1} seconds")
