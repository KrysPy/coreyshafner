import requests

# DOWNLOADING PNG
r = requests.get('https://imgs.xkcd.com/comics/python.png')

with open('comic.png', 'wb') as file:
    file.write(r.content)


# CHECKING IF WE GET A GOOD RESPONSE - in HTTP status codes
print(r.status_code)
print(r.ok)

# INFORMATION ABOUT PAGE
print(r.headers)

# requesting sending params instead of typing: 'https://httpbin.org/get?page=2&count=25'
payload = {'page': 2, 'count': 25}
r2 = requests.get('https://httpbin.org/get', params=payload)
print(r2.text)
print(r2.url)

# we getting json response back from request
print(r2.json())

# posting some data
payload = {'username': 'corey', 'password': 'testing'}
r2 = requests.post('https://httpbin.org/post', data=payload)

r2_dict = r2.json()
print(r2_dict['form'])
print('---------------------------------------------------------------------------------------------------------')
# ---------------------------------------------------------------------------------------------------------

# FROM REAL PYTHON PAGE

response = requests.get('https://api.github.com')
print(response.status_code)
# to see the response content in bytes:
print(response.content)
# to see the response in using e.g. encoding UTF-8
