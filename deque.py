from collections import deque

# is faster in adding elements in the begining and ending of the list

d = deque("hello")
d.append('4')
d.appendleft('3')
# d.pop()
# d.popleft()
# d.clear()
d.extend('elo')

print(d)