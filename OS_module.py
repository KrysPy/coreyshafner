import os
from datetime import datetime


print(os.getcwd())

os.chdir('C:\\Users\\krstn\\Desktop')

os.rmdir('C:\\Users\\krstn\\Desktop\\new_dir')

os.mkdir('new_dir')

print(os.listdir())

print(os.stat('sample.txt'))
last_mod_time = os.stat('sample.txt').st_mtime
print(datetime.fromtimestamp(last_mod_time))

for dirpath, dirname, filename in os.walk(os.getcwd()):
    print("Current Path: ", dirpath)
    print("Directories: ", dirname)
    print("Files: ", filename, "\n")

print(os.environ.get('HOMEPATH'))
os.chdir(os.environ.get('HOMEPATH'))
print(os.listdir())
file_path = os.path.join(os.environ.get('HOMEPATH'), 'Pictures')
print(file_path)

print(os.path.basename('\\tmp\\test.txt'))
print(os.path.dirname('\\tmp\\test.txt'))
print(os.path.split('\\tmp\\test.txt'))
print(os.path.exists('\\tmp\\test.txt'))
print(os.path.splitext('\\tmp\\test.txt'))
