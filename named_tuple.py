from collections import namedtuple

# RGB (R, G, B) - tuple representation
color_tuple = (55, 155, 255)
# to print red value
print(color_tuple[0])

# RGB (R, G, B) - dict representation
color_dict = {'red': 55, 'green': 155, 'blue': 255}
# to print red value
print(color_dict['red'])

# RGB (R, G, B) - namedtuple representation
Color = namedtuple('Color', ['red', 'green', 'blue'])
color = Color(red=55, green=155, blue=255)
# when we want to create new color we just type:
white = Color(255, 255, 255)

print(white.red)
print(color.red)
