try:
    f = open('Test.txt')
    # var = basf
    if f.name == "Test.txt":
        raise Exception
except FileNotFoundError:
    print("Error, such file does not exist!")
except Exception as e:
    print(e)
else:
    print(f.read())
    f.close()
finally:
    print("Executing finalluy...")
