# first class function allow us to treat a function like any other object

def square(x):
    return x * x


def cube(x):
    return x * x * x


f = square

print(square)
print(f(5))


# ----------------------------------------------------------
def my_map(func, arg_list):
    result = []
    for i in arg_list:
        result.append(func(i))
    return result


# PASSING FUNCTION TO FUNCTION
# when we add parenthesis that means that we want tu try execute the function
squares = my_map(square, [1, 2, 3, 5, 6])
squares2 = my_map(cube, [1, 2, 3, 5, 6])
print(squares)
print(squares2)


# RETURNING FUNCTION BY FUNCTION
def logger(msg):

    def log_message():
        print("Log", msg)

    return log_message


log_hi = logger('HI ')
print(log_hi)
log_hi()

# why return function from function?
# - to login
def html_tag(tag):

    def wrap_text(msg):
        print(('<{0}>{1}</{0}>').format(tag, msg))

    return wrap_text


print_h1 = html_tag('h1')
print_h1('Test Headline')

print_p = html_tag('p')
print_p('Test paragraph')
