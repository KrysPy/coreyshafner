import os
from contextlib import contextmanager

# file = open('sample.txt', 'w')
# file.write("Loresd sdfsd sdfs sd")
# file.close()


# class OpenFile:
#     def __init__(self, filename, mode):
#         self.filename = filename
#         self.mode = mode
#
#     def __enter__(self):
#         self.file = open(self.filename, self.mode)
#         return self.file
#
#     def __exit__(self, exc_type, exc_val, exc_tb):
#         self.file.close()
#
#
# with OpenFile('sample.txt', 'w') as file:
#     file.write("sdsd")
#
# print(file.closed)

################## Using ContextLib ##########################
# @contextmanager
# def open_file(file, mode):
#     try:
#         f = open(file, mode)
#         yield f
#     finally:
#         f.close()
#
#
# with open_file('sample.txt', 'w') as f:
#     f.write("Lotem casrv wevweev rvecd vwc")
# print(f.closed)

# PRACTICAL EXAMPLE

# INSTEAD OF:
# cwd = os.getcwd()
# os.chdir('Sample-Dir-One')
# print(os.listdir())
# os.chdir(cwd)
#
# cwd = os.getcwd()
# os.chdir('Sample-Dir-Two')
# print(os.listdir())
# os.chdir(cwd)

# WE CAN MAKE:
@contextmanager
def change_dir(destination):
    try:
        cwd = os.getcwd()
        os.chdir(destination)
        yield
    finally:
        os.chdir(cwd)


# we just yield without any value so we do not have to write 'as file' in context manager
with change_dir('Sample-Dir-One'):
    print(os.listdir())

with change_dir('Sample-Dir-Two'):
    print(os.listdir())