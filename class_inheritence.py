class Employee:

    raise_amount = 1.04

    def __init__(self, first_name, last_name, pay):
        self.first_name = first_name
        self.last_name = last_name
        self.pay = pay
        self.email = first_name + '.' + last_name + '@company.com'

    def fullname(self):
        return f'{self.first_name} {self.last_name}'

    def apply_raise(self):
        self.pay = int(self.pay * self.raise_amount)


class Developer(Employee):
    
    raise_amt = 1.10
    
    def __init__(self, first_name, last_name, pay, programming_language: str):
        super().__init__(first_name, last_name, pay)
        self.programming_language = programming_language


class Manager(Employee):
    def __init__(self, first_name, last_name, pay, employees=None):
        super().__init__(first_name, last_name, pay)
        if employees is None:
            self.employees = []
        else:
            self.employees = employees
    
    def add_emp(self, employee: Employee):
        if employee not in self.employees:
            self.employees.append(employee)

    def remove_emp(self, employee: Employee):
        if employee in self.employees:
            self.employees.remove(employee)
    
    def print_emp(self):
        for emp in self.employees:
            print(' --> ', emp.fullname())

if __name__=='__main__':
    dev_1 = Developer('Corey', 'Shafer', 50000, 'Python')
    dev_2 = Developer('Jane', 'Shafer', 60000, 'Java')

    mgr_1 = Manager('Sue', 'Smith', 90000, [dev_1])

    mgr_1.print_emp()

    mgr_1.add_emp(dev_2)
    
    mgr_1.print_emp()
    
    mgr_1.remove_emp(dev_1)
    
    mgr_1.print_emp()
    
    print(isinstance(dev_1, Manager))