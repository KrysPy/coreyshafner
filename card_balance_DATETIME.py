import datetime
import calendar

balance = 5000
interest_rate = 13 * .01
monthly_payment = 500

current_date = datetime.date.today()

# looking how many days is in current month
# (first day (0-monday, 1 thu etc), amount_of_days) = monthrange return that tuple
# print(days_in_current_month)
days_in_current_month = calendar.monthrange(current_date.year, current_date.month)[1]

days_till_end_month = days_in_current_month - current_date.day

# we want to get first day of next month
start_payment_date = current_date + datetime.timedelta(days=days_till_end_month + 1)
end_date = start_payment_date

while balance > 0:
    interest_charge = (interest_rate / 12) * balance
    balance += interest_charge
    balance -= monthly_payment

    # two kind of describe
    # balance = round(balance, 2)
    # if balance < 0:
    #     balance = 0
    balance = 0 if balance < 0 else round(balance, 2)

    print(end_date, balance)

    days_in_current_month = calendar.monthrange(end_date.year, end_date.month)[1]
    end_date = end_date + datetime.timedelta(days=days_in_current_month)


