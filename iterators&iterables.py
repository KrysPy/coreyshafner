# iterable bo mozna uzyc w petli for i iterowac po elementach; obiekt musi zwracac iterator obiekt z dunder
# metody __iter__

# iterator bo ma metode dunder next
# yield

nums = [1, 2, 3]

i_nums = iter(nums)
print(next(i_nums))
print(next(i_nums))
print(dir(i_nums))

for num in nums:
    print(num)

print()


class MyRange:
    def __init__(self, start, end, step):
        self.value = start
        self.end = end
        self.step = step

    def __iter__(self):
        return self

    def __next__(self):
        if self.value >= self.end:
            raise StopIteration
        current_val = self.value
        self.value += self.step
        return current_val


range_nums = MyRange(1, 10, 2)
# for num in range_nums:
#     print(num)

print(next(range_nums))
print(next(range_nums))
print(next(range_nums))
print(next(range_nums))


def my_range(start, end, step):
    current_val = start
    while current_val < end:
        yield current_val
        current_val += step


print()
with_yield = my_range(1, 10, 3)
print(next(with_yield))
print(next(with_yield))
for num in with_yield:
    print(num)

