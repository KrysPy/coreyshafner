nums = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
names = ['Bruce', 'Clark', 'Peter', 'Logan', 'Wade']
heroes = ['Batman', 'Superman', 'Spiderman', 'Wolverine', 'Deadpool']

# +++++++++++++++++ LIST COMPREHENSION +++++++++++++++++

# I want 'n' for each 'n' in nums
# my_list = []
# for n in nums:
#     my_list.append(n)
# print(my_list)

# my_list = [n for n in nums]
# print(my_list)

# DOING THE SAME USING MAP AND LAMBDA
# my_list = map(lambda n: n*n, nums)
# print(list(my_list))

# ------------------------------------------------------------------------------------------------------
# my_list = [n for n in nums if n % 2 == 0]
# print(my_list)

# ------------------------------------------------------------------------------------------------------
# I want a (letter, num) pair for each letter in 'abcd' and each number in '0123'
# my_list = []
# for letter in 'abcd':
#     for num in range(4):
#         my_list.append((letter, num))
# print(my_list)

# my_list = [(letter, num) for letter in 'abcd' for num in range(4)]
# print(my_list)
# ------------------------------------------------------------------------------------------------------

# +++++++++++++++++ DICT COMPREHENSION +++++++++++++++++

# I want a dict{'name': 'hero'} for each name, hero in zip(names, hero)
# my_dict = {}
# print(f"Zipped lists: {list(zip(names, heroes))}")
# for name, hero in zip(names, heroes):
#     my_dict[name] = hero
# print(my_dict)

# my_dict = {name: hero for name, hero in zip(names, heroes) if name != 'Peter'}
# print(my_dict)

# ------------------------------------------------------------------------------------------------------

# +++++++++++++++++ SET COMPREHENSION +++++++++++++++++
nums = [1, 1, 2, 1, 3, 4, 3, 4, 5, 5, 6, 7, 8, 7, 9, 9]
# my_set = set()
# for n in nums:
#     my_set.add(n)
# print(my_set)

my_set = {n for n in nums}
print(my_set)

