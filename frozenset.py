x = frozenset([1, 2, 3])
print(x)

y = frozenset("hey")
print(y)

a = {4, 5, 6, frozenset([1])}
print(a)