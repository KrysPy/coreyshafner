import datetime
import pytz

dat = datetime.date(2016, 7, 24)
print(dat)

tday = datetime.date.today()
print(tday.year)            # we can also get a day, month or all date
print(tday)                 # full date
print(tday.weekday())       # monday is 0; sunday 6
print(tday.isoweekday())    # monday is 1; sunday 7

# TIMEDELTA
tdelta = datetime.timedelta(days=7)
print(tday + tdelta)        # we can adding and subtracting days

# date2 = date1 + timedelta
# timedelta = date1 + date 2
bday = datetime.date(2020, 9, 24)
till_bday = bday - tday
print(till_bday.days)

t = datetime.time(9, 30, 45, 10000)
print(t)                    # we can add .hour etc. like in date when we can get hours. minutes etc.....
t = datetime.datetime.today()  # datetime.date - access to date; datetime.time - access to time; datetime.datetime - all
print(t)                       # also we can get just hours, year etc.

# USING PYTZ
dt = datetime.datetime(2016, 7, 27, 12, 34, 45, tzinfo=pytz.UTC)
print(dt)

dt_now = datetime.datetime.now(tz=pytz.UTC)
dt_utcnow = datetime.datetime.utcnow().replace(tzinfo=pytz.UTC)
print(dt_now)
print(dt_utcnow)

# CONVERT TO DIFFERENT TIMEZONE
dt_mnt = dt_utcnow.astimezone(pytz.timezone('Europe/Warsaw'))
print(dt_mnt)
print()

# TO GET ALL TIMEZONES
# for tz in pytz.all_timezones:
#     print(tz)
dt_mnt = datetime.datetime.now()
mtn_tz = pytz.timezone('US/Mountain')
dt_mnt = mtn_tz.localize(dt_mnt)
print(dt_mnt)

# display date times
dt_mnt = datetime.datetime.now(tz=pytz.timezone('US/Mountain'))
print(dt_mnt.strftime('%B %d, %Y'))

# convert string to datetime format
dt_str = 'July 26, 2016'
dt = datetime.datetime.strptime(dt_str, '%B %d, %Y')
print(dt)

# strftime - convert datetime to  string
# strptime - string to datetime

