import re

emails = '''
CoreyMSchafer@gmail.com
corey.schafer@university.edu
corey-321-schafer@my-work.net
'''

urls = '''
https://www.google.com
http://coreyms.com
https://youtube.com
https://www.nasa.gov
'''

text_to_search = '''
abcdefghijklmnopqurtuvwxyz
ABCDEFGHIJKLMNOPQRSTUVWXYZ
1234567890
Ha HaHa
MetaCharacters (Need to be escaped):
. ^ $ * + ? { } [ ] \ | ( )
coreyms.com
321-555-4321
123.555.1234
123*555*1234
800-555-1234
900-555-1234
Mr. Schafer
Mr Smith
Ms Davis
Mrs. Robinson
Mr. T

cat 
mat 
bat
pat
'''

# what is in brackets is find once (8 or 9; - or .)
pattern = re.compile(r'[89]00[-.]\d\d\d[-.]\d\d\d\d')

# [^......] - dash (^) in brackets mean that we want match without this in brackets with dash
pattern2 = re.compile(r'[^b]at')

# {3} - how match  we want to find of for e.g \d digit
pattern3 = re.compile(r'\d{3}.\d{3}.\d{4}')

# when we add ? after /. we said that dot ist optional and can occur 0 or 1
# after dot we looking for one space (\s) then big letter and then 0 or more letters (\w*)
pattern4 = re.compile(r'Mr\.?\s[A-Z]\w*')

# in parenthesis we put the looking group, | - means or;
pattern5 = re.compile(r'(Mr|Ms|Mrs)\.?\s[A-Z]\w*')

# example for finding emails
pattern6 = re.compile(r'[A-Za-z.-]+@[A-Za-z-]+\.(com|edu|net)')

# we can group string
pattern7 = re.compile(r'https?://(www\.)?(\w+)(\.\w+)')


# with open('data', 'r', encoding='utf-8') as file:
#     contents = file.read()
#
#     new = [i for i in pattern.finditer(contents)]
#
#     for i in new:
#         print(i)

new = [i for i in pattern7.finditer(urls)]

# when we want work with group we add .group
for i in new:
    print(i.group(3))

# we use sub to substitute group 2 and 3
subbed_urls = pattern7.sub(r'\2\3', urls)

print(subbed_urls)

# findall - return just matches as a list
# when we have grouped pattern we get list of tuples with group
matches = pattern7.findall(urls)
print(matches)

# match method - return first match BUT JUST AT THE BEGINING or return None
# we have search method but that just return first find match in all string not just begin
# otherwise return None

sentence = ' sTaRt of string'
# we can add flag re.IGNORECASE and that ignore whether letter is lower or upper
pattern8 = re.compile(r'start', re.IGNORECASE)
search = pattern8.search(sentence)
print(search)
