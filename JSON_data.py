# JavaScript Object Notation - JSON
import json
from urllib.request import urlopen

with open('states.json', 'r') as json_file:
    data = json.load(json_file)

print(data)

for state in data['states']:
    del state['name']

with open('new_states.json', 'w') as json_file:
    json.dump(data, json_file, indent=2)

